import numpy as np
import tensorflow as tf
import cv2

# load the model in the application
model = tf.keras.models.load_model("cat_dog_img_classification.model")


def test_model(filepath):
    categories = ['cat', 'dog']
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    new_array = cv2.resize(img_array, (50, 50))
    img_std = new_array.reshape(-1, 50, 50, 1).astype('float32') / 255

    output = model.predict(img_std)
    max_percentage = max(output[0])
    # print(max_percentage)

    position = np.where(output[0] == max_percentage)
    # print(position[0][0])
    print(f"----The image is of {categories[position[0][0]]}----")
    # print(output)
    image = cv2.resize(img_array, (720, 650))
    cv2.imshow("image", image)
    cv2.waitKey(1000)
    cv2.destroyAllWindows()


image_path = input("Enter the path of image: ")
image_path.replace("\\", "\\\\")
test_model(image_path)
